import unittest
from clase_persona import Paciente, Medico

class Testpersona(unittest.TestCase):
    def test_informacion_paciente(self):
        paciente1 = Paciente("cancer", "Mariano", "Martínez", "12 de octubre de 1972", "51654475Q")

        self.assertEqual(str(paciente1), "Nombre y apellidos: Mariano Martínez, fecha de nacimiento: 12 de octubre de 1972, DNI: 51654475Q")
    
    def test_historial_clinico(self):
        paciente1 = Paciente("cancer", "Mariano", "Martínez", "12 de octubre de 1972", "51654475Q")

        self.assertEqual(paciente1.ver_historial_clinico(), "Historial clinico de Mariano: cancer")

    def test_citas(self):
        medico1 = Medico("cardiologia, traumatologia", "ginecologia", "Waldemar", "Stegierski Moreno", "1998", "83435728E")

        self.assertEqual(medico1.consultar_citas(), "Las citas de Waldemar son: cardiologia, traumatologia")

if __name__ == '__main__':
    unittest.main() 