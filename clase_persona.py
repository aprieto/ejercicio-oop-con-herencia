class Persona:
    def __init__(self, name, surname, birth, DNI):
        self._name = name
        self._surname = surname
        self._date = birth
        self._DNI = DNI
    
    def __str__(self):
        return f"Nombre y apellidos: {self._name} {self._surname}, fecha de nacimiento: {self._date}, DNI: {self._DNI}"
    
    def setName(self, name):
        self._name = name
    def getName(self):
        return self._name
    
    def setSurname(self, surname):
        self._surname = surname
    def getSurname(self):
        return self._surname
    
    def setDate(self, birth):
        self._date = birth
    def getDate(self):
        return self._date
    
    def setDNI(self, DNI):
        self._DNI = DNI
    def getDNI(self):
        return self._DNI 

class Paciente(Persona):
    def __init__(self, historial_clinico, n, s, b, DNI):
        super().__init__(n, s, b, DNI)
        self.historial_clinico = historial_clinico
    
    def ver_historial_clinico(self):
        return f"Historial clinico de {super().getName()}: {self.historial_clinico}"

class Medico(Persona):
    def __init__(self, c, e, n, s, b, DNI):
        super().__init__(n, s, b, DNI)
        self.especialidad = e
        self.citas = c

    def consultar_citas(self):
        return f"Las citas de {super().getName()} son: {self.citas}"

# Pruebas para ver si funciona el codigo:
"""paciente1 = Paciente("cancer", "Mariano", "Martínez", "12 de octubre de 1972", "51654475Q")
print(paciente1)
paciente2 = Paciente("sindrome de down, gonorrea", "Paloma", "Arribas", "2005", "83476826K")
print(paciente2)
medico1 = Medico("cardiologia, traumatologia", "ginecologia", "Waldemar", "Stegierski Moreno", "1998", "83435728E")
print(medico1)
medico2 = Medico("psicologo", "dislexia", "Berta", "Babiano", "2004", "85209785J")
print(medico2)

print(paciente1.ver_historial_clinico())
print(paciente2.ver_historial_clinico())
print(medico1.consultar_citas())
print(medico2.consultar_citas())"""